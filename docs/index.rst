.. Drift documentation master file, created by
   sphinx-quickstart on Sun Feb  9 17:03:56 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Drift
================

Drift help developers write user friend CMSes in Django.

Similar in design to the Django admin, Drift allows the creation of content classes which define behavoirs around how editor can change their content. It makes content easier to edit while keeping the developer in control. Content can be something similar to a flat page where all the content is stored as HTML or structured data with multiple fields of different types.

Install
=======

Drift requires Django. Currently it is tested with Django 1.6 and 1.7.

Bootstrap 3 is recommended for the provided templates, though you could define your own bootstrap compatable styles and not use bootstrap or overide the template. It should be possible to define add on apps which provide standard template for other style packages like Foundation.

::

    pip install drift

Edit your ``settings.py`` and add ``'drift'`` to your ``INSTALLED_APPS``.

In Django 1.6 or older you need add the following to your ``urls.py`` file so that Drift can find and use your ``content.py`` file which define your content classes.

::

    from drift import content
    content.autodiscover()



Contrib
=======

Basic examples of content types are included in ``drift.contrib`` which are usable and provide examples of what is required to make a complete content type.

``drift.contrib.flatpages``
---------------------------

Very similar to ``django.contrib.flatpages``, ``drift.contrib.flatpages`` provides a simple database stored page which can be edited directly using Drift.

To use ``drift.contrib.flatpages`` add it to your ``INSTALLED_APPS`` in your ``settings.py``.

Then add a url to your ``urls.py`` file indicating where you want flatpages to be viewed.

::
    url(r'^blog/', include('drift.contrib.flatpages.urls')),

Since Drift is built around the idea of editing your content directly, creating the first page can be a bit tricky. I would recommend doing it through the admin interface. This will get you familiar with the raw flatpage model as well.

Once you have created your first page you can create new pages and edit you pages can going to them directly while signed in and use the Drift interface.

Contents:

.. toctree::
   :maxdepth: 2



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

