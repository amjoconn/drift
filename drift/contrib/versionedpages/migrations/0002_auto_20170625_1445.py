# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('versionedpages', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='page',
            name='owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, verbose_name='owner', to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='page',
            name='published_version',
            field=models.ForeignKey(related_name='published_for', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='versionedpages.Version', null=True),
        ),
        migrations.AlterField(
            model_name='version',
            name='editor',
            field=models.ForeignKey(related_name='versionpages_versions', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
