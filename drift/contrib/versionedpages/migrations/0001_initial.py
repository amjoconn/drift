# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=512, verbose_name='title', blank=True)),
                ('url', models.CharField(unique=True, max_length=200, verbose_name='URL')),
                ('template_name', models.CharField(help_text="Example: 'versionedpages/contact_page.html'. If this isn't provided, the system will use 'versionedpages/default.html'.", max_length=70, verbose_name='template name', blank=True)),
                ('is_published', models.BooleanField(default=False, verbose_name='is published')),
                ('published', models.DateTimeField(null=True, verbose_name='published', blank=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='created')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='updated')),
                ('owner', models.ForeignKey(verbose_name='owner', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['published', 'created'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Version',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_saved', models.BooleanField(default=False, verbose_name='is saved')),
                ('merged', models.BooleanField(default=False, verbose_name='merged')),
                ('html', models.TextField(verbose_name='html', blank=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='created')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='updated')),
                ('editor', models.ForeignKey(related_name='versionpages_versions', to=settings.AUTH_USER_MODEL)),
                ('page', models.ForeignKey(to='versionedpages.Page')),
                ('parent', models.ForeignKey(blank=True, to='versionedpages.Version', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='page',
            name='published_version',
            field=models.ForeignKey(related_name='published_for', blank=True, to='versionedpages.Version', null=True),
            preserve_default=True,
        ),
    ]
